<?php

// autocarga de clases
spl_autoload_register(function ($clase) {
    require 'clases/' . $clase . '.php';
});

$alumno=new Persona("20202020");
var_dump($alumno);
$profesor=new Persona("jorge",5,"h");
var_dump($profesor);

$profesor->setPeso(89);
$profesor->setAltura(180);
echo $profesor->calcularIMC();
echo "<br>";
echo (int)$profesor->esMayorDeEdad();

echo $profesor;

$profesor->setDni("20211818");
var_dump($profesor);

$profesor->setSexo("X");
var_dump($profesor);
