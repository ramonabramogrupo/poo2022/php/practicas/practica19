<?php

/**
 * Description of Persona
 *
 * @author Profesor Ramon
 */
class Persona {
    const DEBAJO = -1;
    const IDEAL = 0;
    const ENCIMA = 1;
    private ?string $nombre=null;
    private int $edad=0;
    private ?string $dni=null;
    private string $sexo;
    private int $peso=0;
    private int $altura=0;
    
    public function __construct(...$datos) {
        switch(count($datos)){ // cuantos argumentos me has pasado???
            case 1:
                $this->__construct1($datos[0]); // llamo al constructor pasando dni
                break;
            case 3:
                $this->__construct3($datos[0],$datos[1],$datos[2]);
                break;
        }
    }
    
    private function __construct1(string $dni){
        $this->setDni($dni);
    }
    
    private function __construct3(string $nombre,int $edad,string $sexo){
        $this->nombre=$nombre;
        $this->edad=$edad;
        $this->setSexo($sexo);
    }
    
    public function calcularIMC(): int{
        $imc=$this->peso/($this->altura/100)**2;
        if($imc<20){
            return self::DEBAJO;
        }elseif($imc<=25){
            return self::IDEAL;
        }else{
            return self::ENCIMA;
        }
    }
    
    public function getNombre(): ?string {
        return $this->nombre;
    }

    public function getEdad(): int {
        return $this->edad;
    }

    public function getDni(): ?string {
        return $this->dni;
    }

    public function getSexo(): string {
        return $this->sexo;
    }

    public function getPeso(): int {
        return $this->peso;
    }

    public function getAltura(): int {
        return $this->altura;
    }

    public function setNombre(?string $nombre): void {
        $this->nombre = $nombre;
    }

    public function setEdad(int $edad): void {
        $this->edad = $edad;
    }

    public function setDni(?string $dni): void {
        $this->dni = $dni;
        $this->generaDNI();
    }

    public function setSexo(string $sexo): void {
        $this->sexo = $sexo;
        $this->comprobarSexo();
    }

    public function setPeso(int $peso): void {
        $this->peso = $peso;
    }

    public function setAltura(int $altura): void {
        $this->altura = $altura;
    }
    
    public function esMayorDeEdad(): bool{
        if($this->edad>=18){
            return true;
        }else{
            return false;
        }
    }

    public function __toString() {
        $resultado= "<br>" . $this->nombre . "<br>";
        $resultado.=$this->edad . "<br>";
        $resultado.=$this->altura . "<br>";
        $resultado.=$this->peso . "<br>";
        $resultado.=$this->sexo . "<br>";
        $resultado.=$this->dni . "<br>";
        return $resultado;
    }
    
    private function generaDNI() : void{
        $letras=["T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E"];
        
        $posicion=$this->dni%23;
        $letraCalculada=$letras[$posicion];
        
        $this->dni = $this->dni . $letraCalculada;
        
    }
    
    private function comprobarSexo(){
        if(!(strtoupper($this->sexo) == "H" || strtoupper($this->sexo) == "M")){
            $this->sexo="H";
        }
    }


    
    
}
