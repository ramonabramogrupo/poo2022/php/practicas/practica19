<?php

/**
 * Description of Punto
 *
 * @author Profesor Ramon
 */
class Punto {
    private float $x;
    private float $y;
    public function __construct(float $x, float $y) {
        $this->x = $x;
        $this->y = $y;
    }
    public function getX(): float {
        return $this->x;
    }

    public function getY(): float {
        return $this->y;
    }

    public function setX(float $x): void {
        $this->x = $x;
    }

    public function setY(float $y): void {
        $this->y = $y;
    }
    
    public function igual(Punto $puntoA) : string{
        if($this->x==$puntoA->x && $this->y==$puntoA->y){
            return "Iguales";
        }else{
            return "No son iguales";
        }
    }
    
    /*
     * calcula la distancia con respecto al origen
     */
    
//    public function distancia():float{
//        $resultado=sqrt(pow($this->x,2)+$this->y**2);
//        return $resultado;
//    }
    
    /**
     * 
     *
     * calcula la distancia con respecto a otro punto
     */
    
//    public function distancia(Punto $puntoA):float{
//        $resultado=sqrt(pow($this->x-$puntoA->x,2)+pow($this->y-$puntoA->y,2));
//        return $resultado;
//    }
    
    public function distancia(Punto $puntoA=null) {
        if(is_null($puntoA)){ // no me pasas nada, tengo que calcular la distancia con respecto al origen
        $resultado=sqrt(pow($this->x,2)+$this->y**2);
        return $resultado;
            
        }else{ // si me pasas algo calculo la distancia con respecto al punto que me pasas
            
        $resultado=sqrt(pow($this->x-$puntoA->x,2)+pow($this->y-$puntoA->y,2));
        return $resultado;
        }
    }
    
//    public function distancia(Punto $puntoA=null) {
//        if(is_null($puntoA)){ // no me pasas nada, tengo que calcular la distancia con respecto al origen
//        $puntoA=new Punto(0,0);
//        }
//            
//        $resultado=sqrt(pow($this->x-$puntoA->x,2)+pow($this->y-$puntoA->y,2));
//        return $resultado;
//        
//    }

    
    
   

}
