<?php

// autocarga de clases
spl_autoload_register(function ($clase) {
    require 'clases/' . $clase . '.php';
});

$punto1=new Punto(20, 10);

$punto2=new Punto(10,10);

echo "<br>";
echo $punto2->igual($punto1);

echo "<br>";
echo "la distancia del punto 2 al origen:";
echo $punto2->distancia();

echo "<br>";
echo "la distancia del punto 2 al punto 1:";
echo $punto2->distancia($punto1);
